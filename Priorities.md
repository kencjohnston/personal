## Quarterly Priorities
This content is meant to communicate how I intend to allocate my time. I review it weekly, but it should largely remain consistent on the time-scales of quarters.

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Team Development | CDF Reviews, Career Development, Team Engagement, GitLab Values Coaching | 20% |
| Sensing Mechanisms | Internal context gathering, customer interviews, analyst inquiries, competitive review, context setting | 10% |
| Cross Section Product Experience | Think Big-Think Small, Walk-throughs, learning goals, direction content review and creation | 30% |
| GTM Engagement | Use Case alignment, Opportunity Review, Sales Support, PMM/TMM Alignment | 15% |
| Product Performance Indicators | Performance Indicator instrumentation, understanding, goal setting and attainment | 5% |
| External Evangelism | Ops vision, analyst briefings, conference speaking | 5% |
| Core Team Engagement | UX, Development, Quality, Infrastructure shared Performance Indicators and Retrospectives | 5% |
| Personal Growth / Leadership Opportunities | Representing GitLab externally, Representing Product internally | 5% |
| Hiring | Sourcing, interviewing, hiring, onboarding | 5% |

## Quarterly Goals (Current Quarter)
- Back on target to hit annual CMAU goals
- Create a conversion plan for SFUE
- Create a collaborative Ops Section Walk Through

## Weekly Priorities
This content is meant to communicate my priorities on a weekly basis. They should typically be reflected as items in the `Doing` of [my personal issue board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston). It will be updated weekly. As I'm always striving to learn I'll also add a `Learning Goal` each week. You can see [the history](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&assignee_username[]=kencjohnston&label_name[]=Weekly%20Priorities) of my Weekly Priorities setting issues and the [template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Weekly-Priority-Setting-Kenny.md) I use. 

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not completed
* **W** - Waiting for next steps

## 2022-08-01
New Quarter!
1. Hiring!
1. [Analytics Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)
1. [Create a SSOT for Stage Field Syncs](https://gitlab.com/gitlab-com/Product/-/issues/4606)
1. Close out discussion on [allocating scaling investments](https://gitlab.com/gitlab-com/Product/-/issues/4522#note_1042207284) in PLT
1. Summarize [Ops Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/4537)
1. [Start of Quarter Opportunity Review Summary](https://gitlab.com/gitlab-com/Product/-/issues/4623)

## 2022-07-25
Full week.
1. Y - Prepare, present and followup from [Ops Section PI review](https://gitlab.com/gitlab-com/Product/-/issues/4505)
1. Y - [Analytics next steps](https://gitlab.com/gitlab-com/Product/-/issues/3785) with [Sam Kerr](https://gitlab.com/gitlab-com/Product/-/issues/4524)
1. Y - [Ensure we are properly allocating our scaling investments](https://gitlab.com/gitlab-com/Product/-/issues/4522)
1. Y - [Draft product principles for `components`](https://gitlab.com/gitlab-com/Product/-/issues/4549)
1. Y - [Perform Ops Monthly Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/4479)
1. [Finalize Combined Revenue and Cost Model for SFUE](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/88)

## 2022-07-13
Short week, missed priorities setting until end of week.

## 2022-07-05
Short week with US Fourth of July Holidays
1. N - [Make Progress on Combined Revenue and Cost Model for SFUE](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/88)
1. Y - [Continue Outreach to top User Limit Namespaces](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/83)
1. Y - Coordinate SFUE Roll Out Plan prep ahead of comms launch
1. N - [Investigate Dips in Ops Section Contribution to SpO](https://gitlab.com/gitlab-com/Product/-/issues/4447)
1. N - [Ops Section June Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/4403)
1. N - [Finalize Proposal for Ops Section QGRs](https://gitlab.com/gitlab-com/Product/-/issues/4240)

## 2022-06-27
1. Y - [Present and Followup from Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/4345)
1. Y - [Finalize Ops Section FY24 Goals](https://gitlab.com/gitlab-com/Product/-/issues/4195)
1. Y - [Monthly Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/4320)
1. Y - [Assist in Ian's Ramp Up on SFUE](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/86)
1. Y - [Kickoff Creation of Revenue Model for SFUE](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/88#references)
1. Y - [Update Analytics Section Direciton with preferred use cases](https://gitlab.com/gitlab-com/Product/-/issues/4396)
1. Y - Proposal for [Ops Section QGRs](https://gitlab.com/gitlab-com/Product/-/issues/4240)
1. N - Finalize [Forced Prioritization Update](https://gitlab.com/gitlab-com/Product/-/issues/4005)

## 2022-06-20
Short week with Juneteeth observed on Monday and F&F on Friday.
1. Y - Team CDFs
1. Y - [Kickoff Prep](https://gitlab.com/gitlab-com/Product/-/issues/4291)
1. W - [SFUE Hanbook updates with new plan](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/85)
1. Y - [Ops Section PI Review Prep](https://gitlab.com/gitlab-com/Product/-/issues/4345)
1. N - [Update Analytics Section Direciton with preferred use cases](https://gitlab.com/gitlab-com/Product/-/issues/4396)
1. N - Proposal for [Ops Section QBRs](https://gitlab.com/gitlab-com/Product/-/issues/4240)

## 2022-06-13
1. Y - [Adjust and E-Group Review for SFUE Rollout Plan](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/82)
1. Y - E-Group Offsite
1. Y - [Analytics Section Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)

## 2022-06-06
1. Y - [Adjust Rollout Plan for SFUE](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/82)
1. Y - Analyst Briefings for 15
1. Y - Product Leadership Offsite Monday and Tuesday
1. N - PlatformCon on Thursday and Friday
1. Y - [Analytics Section Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)

## 2022-05-30
Short week with Memorial Day Monday.
1. Y - [Ops Section Direction Update](https://gitlab.com/gitlab-com/Product/-/issues/4230)
1. Y - [Ops Section Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/4178)
1. Y - [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/4200)
1. Make progress on SFUE [attribution next steps](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/77) and [setting conversion goals](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/71)
1. [Forced Prioritization and Eng Allocation Followup](https://gitlab.com/gitlab-com/Product/-/issues/4005)
1. Y - [Read Through Product Intelligence Context](https://gitlab.com/gitlab-org/product-intelligence/-/issues/581)
1. Y - [Analytics Section Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)

## 2022-05-23
Shorter week with F&F on Friday, I ended up getting sick and missing three days.
1. Y - [SFUE Announcement Changes](https://gitlab.com/gitlab-com/packaging-and-pricing/pricing-handbook/-/issues/428)
1. Y - Provide output format and request for [FY24 Usage Goals from Ops Section PMs](https://gitlab.com/gitlab-com/Product/-/issues/4195)
1. N- Finalize [April Cost Data review](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/78)
1. N - [Ops Section Direction Update](https://gitlab.com/gitlab-com/Product/-/issues/4230)
1. N - [Ops Section Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/4178)
1. N - Make progress on SFUE [attribution next steps](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/77) and [setting conversion goals](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/71)
1. N - [Forced Prioritization and Eng Allocation Followup](https://gitlab.com/gitlab-com/Product/-/issues/4005)

## 2022-05-16
I'm in Valencia for KubeCon and traveling all day Friday.
1. N - Make progress on SFUE [attribution next steps](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/77) and [setting conversion goals](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/71)
1. W - Finalize [April Cost Data review](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/78)
1. Y - Finalize [Ops Section Borrow Request Retrospective](https://gitlab.com/gitlab-com/Product/-/issues/4082)
1. Y - Complete and Summarize [Ops Section Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/4091)
1. Y - Request [FY24 Usage Goals from Ops Section PMs](https://gitlab.com/gitlab-com/Product/-/issues/4195)

## 2022-05-09
Only in two days this week, traveling for a prospect visit on Wednesday and Thursday and out the following week at a conference.
1. Y - [Prep for Prospect visit](https://gitlab.com/gitlab-com/Product/-/issues/4182)
1. Y - [Prep Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/4192)
1. Y - Finalize and Publish [Quarterly Opportunity Review Highlights](https://gitlab.com/gitlab-com/Product/-/issues/4161)
1. Y - Complete [Clickhouse Service Contract](https://gitlab.com/gitlab-com/Product/-/issues/4100)

## 2022-05-02
1. Y - [Finalize Q2 Adjustments to SFUE Plan](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/76)
1. Y - Attend QBRs
1. Y - [Prep Retrospective for Verify Borrow Request](https://gitlab.com/gitlab-com/Product/-/issues/4082)
1. Y - Begin [AFKA (Engage) Stage Discovery](https://gitlab.com/gitlab-com/Product/-/issues/4113)
1. W - [Set Conversion Goals for SFUE](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/71)
1. Y - [Next steps on SFUE Attribution](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/77)
1. Y - Next Steps after [Bottom 10% Proposal E-Group Review](https://gitlab.com/gitlab-com/Product/-/issues/4057)
1. Y - Progress on [Clickhouse Service Contract](https://gitlab.com/gitlab-com/Product/-/issues/4100)

## 2022-04-25
1. Y - Team CDF Reviews
1. Y - [Q2+ Adjustments to SFUE Cost Plan](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/76)
1. N - [Prep Retrospective for Verify Borrow Request](https://gitlab.com/gitlab-com/Product/-/issues/4082)
1. Y - Progress to [Secure Clickhouse Service Contract](https://gitlab.com/gitlab-com/Product/-/issues/4100)
1. Y - [Finalize -10% Proposal](https://gitlab.com/gitlab-com/Product/-/issues/4057)
1. Y - Prep, Present and Followup from [Ops PI Review](https://gitlab.com/gitlab-com/Product/-/issues/4063)
1. N - [Followup on Forced Prioritization Ops Section PM Discuss](https://gitlab.com/gitlab-com/Product/-/issues/4005)
1. W - Communicate adding an [additional role in the PM ladder](https://gitlab.com/gitlab-com/Product/-/issues/3081)
1. N - [Engage Stage Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785) including beginning [Discovery](https://gitlab.com/gitlab-com/Product/-/issues/4113)

## 2022-04-18
1. N - [Secure Clickhouse Service Contract](https://gitlab.com/gitlab-com/Product/-/issues/4100)
1. N - [Followup on Forced Prioritization Ops Section PM Discuss](https://gitlab.com/gitlab-com/Product/-/issues/4005)
1. N - [Prep Retrospective for Verify Borrow Request](https://gitlab.com/gitlab-com/Product/-/issues/4082)
1. Y - [Q2+ Adjustments to SFUE Cost Plan](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/76)
1. Y - [Follow up on FY24 LRO Free Hosting](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/73)
1. N - [Ops Section Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/3968)
1. Y - Final reviews before submitting for approval adding an [additional role in the PM ladder](https://gitlab.com/gitlab-com/Product/-/issues/3081)
1. Y - [Engage Stage Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)

## 2022-04-11
OoO on PTO

## 2022-04-04
1. Y - Attend and Participate in the E-Group Offsite
1. Y - [Prepare content for and present Roadmap for customer roadmap review](https://docs.google.com/document/d/1bhrr_3bZpclPgeKaHjFHYHtklpdZ14u6C-JRXacztk0/edit)
1. N - [Action the Verify Breaking Changes Borrow Request](https://gitlab.com/gitlab-com/Product/-/issues/3852)
1. N - [Followup on Forced Prioritization Ops Section PM Discuss](https://gitlab.com/gitlab-com/Product/-/issues/4005)
1. Y - Prep Coverage Issue
1. Y - [Work on Ops Section Pitch Deck](https://gitlab.com/gitlab-com/corporate-development/-/issues/27)
1. N - [Ops Section Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/3968)
1. W - Final reviews before submitting for approval adding an [additional role in the PM ladder](https://gitlab.com/gitlab-com/Product/-/issues/3081)
1. W - [Engage Stage Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)

## 2022-03-28
1. Y - Finalize [Cost Neutral Headcount Swap](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13185)
1. Y - Action the [Clickhouse Borrow Request](https://gitlab.com/gitlab-com/Product/-/issues/3897)
1. Y - Final reviews before submitting for approval adding an [additional role in the PM ladder](https://gitlab.com/gitlab-com/Product/-/issues/3081)
1. W - [Engage Stage Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)
1. N - [Followup on Forced Prioritization Ops Section PM Discuss](https://gitlab.com/gitlab-com/Product/-/issues/4005)
1. Y - Create a plan to set [SFUE Conversion Targets](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/71)
1. Y - Update [Ops Section Use Case Content](https://gitlab.com/gitlab-com/corporate-development/-/issues/27) and [Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/3968)

## 2022-03-21
Short week, I'm OoO W-Th and we have F&F Day on Friday.
1. Y - Prepare, present and followup from the [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3933)
1. Y - Prepare and present for [Monthly Release Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/3917)
1. Y - Prep [Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/3975)
1. Y - Work on [a possible Cost Neutral HC Swap for Support Monitor:Observability](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13185)

## 2022-03-14
I'm attending Virtual SKO this week so my availabilty is somewhat limited.

1. Y - Attend/Present at Virtual SKO
1. Y - Update, Summarize and Review [Ops Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/3855)
1. Y - Finalize [Borrow Request for Clickhouse Acceleration](https://gitlab.com/gitlab-com/Product/-/issues/3897)
1. Y - [Ensure Rapid Action Structure Gets Started for SSOT](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/65)
1. Y - [Create a Sub-Project for Identifying Company Free Users](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/68)
1. Y - [Engage Stage Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)
1. N - Setup [Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/3758)
1. Y - Work on [Proposal for next step in PM ladder](https://gitlab.com/gitlab-com/Product/-/issues/3081)
1. N - [Prep and Deliver Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/3917)

## 2022-03-07
1. Y - [Ensure Ops Leadership Regularly Tracks Security Issues](https://gitlab.com/gitlab-com/Product/-/issues/3912)
1. Y - Create a [SFUE Sub-Project for Optimizing Top-Level Group Flows](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/66)
1. Y - [Update SFUE Comms Plan to include Impacts and Cohorts](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/67)
1. Y - Kickoff [Rapid Action for SSOT on Cost Data](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/65)
1. Y - Prep and Conduct [SaaS Free User Efficiency Kickoff](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/61)
1. N - [Engage Stage next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)
1. N - Schedule [Monitor:Observability Vision Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/3893)
1. Y - Next steps for [making health analysis visible to product](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/880)
1. Y - [Outreach to High Usage Registry Transfer Users](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/50)
1. Y - Next Steps on [Ops Section Direction Review](https://gitlab.com/gitlab-com/Product/-/issues/3792)

## 2022-02-28
1. Y - Close Out [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3830)
1. Y - Record Product SKO Content
1. Y - Hold [Ops Section Direction Review](https://gitlab.com/gitlab-com/Product/-/issues/3792)
1. Y - Plan [Monitor:Observability Vision Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/3893)
1. N - [Engage Stage Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)
1. Y - Schedule [SaaS Free User Efficiency Kickoff](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/61)

## 2022-02-21
Short week with US Holiday on Monday and F&F on Friday

1. Y Finalize [Product Keynote SKO Content](https://gitlab.com/gitlab-com/marketing/sales-kick-off/fy22-sales-kick-off/-/issues/78) and X - [CI/CD Competitive SKO Content](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/1099)
1. Y - Prep for Director of Product, Dev interviews
1. Y - [Product Analytics Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)
1. Y - Prep for and Delivery [Ops PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3830)
1. N - Schedule [SaaS Free User Efficiency Kickoff](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/61)

## 2022-02-14
Short Week - I'm OoO on a Family Ski Trip [2022-02-16 - 2022-02-18](https://gitlab.com/gitlab-com/Product/-/issues/3811). 

1. Draft Y - [Product Keynote SKO Content](https://gitlab.com/gitlab-com/marketing/sales-kick-off/fy22-sales-kick-off/-/issues/78) and N - [CI/CD Competitive SKO Content](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/1099)
1. Y - [Attend QBRs](https://gitlab.com/gitlab-com/Product/-/issues/3720) on Monday & Tuesday
1. N - [Product Analytics Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)
1. Schedule Y - [Ops Section Direction Review](https://gitlab.com/gitlab-com/Product/-/issues/3792) and N - [SaaS Free User Efficiency Kickoff](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/61)
1. Y - [Next Outreach to High Transfer Users](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/50)

## 2022-02-07
1. Y - Update [Pending SaaS Free User Decisions](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/6#interrobang-pending-decisions) and provide [Weekly Async Update](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#weekly-async-updates)
1. Y - [Attend QBRs](https://gitlab.com/gitlab-com/Product/-/issues/3720) on Tuesday & Friday
1. N - Draft [SKO Content](https://gitlab.com/gitlab-com/marketing/sales-kick-off/fy22-sales-kick-off/-/issues/78)
1. Y - [Product Analytics Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3785)
1. Y - [Prep Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/3811)
1. Y - Continue [outreach to high cost registry users](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/50)
1. Y - [Dig into "Other Hosting Services" GitLab.com Cost Bucket](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/55)

## 2022-01-31
1. Y - Update and make progress on [Pending SaaS Free User Decisions](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/6#interrobang-pending-decisions)
1. N - [Setup Collaborative Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/3758)
1. Y - Continue [outreach to high cost registry users](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/55)
1. Y - [Dig into "Other Hosting Services" GitLab.com Cost Bucket](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/55)
1. Y - [Prep for QBRs](https://gitlab.com/gitlab-com/Product/-/issues/3720)
1. Y - First steps to [Propose an Engage/Feedback Stage](https://gitlab.com/gitlab-com/Product/-/issues/3626)
1. Y - Make progress on [Principal+ PM Position](https://gitlab.com/gitlab-com/Product/-/issues/3081) and Y - [Updated Director CDF Content](https://gitlab.com/gitlab-com/Product/-/issues/3757)

## 2022-01-24
1. Y - Prep and Deliver CDF Updates
1. Y - Prep and Complete [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3695)
1. Y - [Ops Section Validation Track Review](https://gitlab.com/gitlab-com/Product/-/issues/3707)
1. Y - Finalize [Pending SaaS Free User Decisions](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/6#interrobang-pending-decisions)
1. Y - [Begin Outreach to High Usage Registry Transfer Users](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/50)
1. Y - Continue work on [Bottoms Up Model for GitLab.com](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/48)
1. N - [Propose an Engage/Feedback Stage](https://gitlab.com/gitlab-com/Product/-/issues/3626)
1. Y - [Set SMAU Goals to hit 3.8M CMAU in FY23](https://gitlab.com/gitlab-com/Product/-/issues/3650)

## 2022-01-17
1. Y - Prep and Deliver [Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/3645)
1. N - Finalize [Pending SaaS Free User Decisions](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/6#interrobang-pending-decisions)
1. Y - Update SaaS Free User Direction with [Expected Success Measures](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/21)
1. Y - [Create Change Management Process for Finance Cost Model](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/30)
1. Y - Begin [Crafting Bottoms Up Model for GitLab.com](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/48)
1. N - Follow up from Opstrace acquisition with content - [Direction](https://gitlab.com/gitlab-com/corporate-development/-/issues/27), [Community](https://gitlab.com/gitlab-com/corporate-development/-/issues/28)
1. Y - Review, Summarize and Update Directions for [Developer Led Landscape](https://gitlab.com/gitlab-com/Product/-/issues/3674)
1. N - [Propose an Engage/Feedback Stage](https://gitlab.com/gitlab-com/Product/-/issues/3626)
1. N - [Set SMAU Goals to hit 3.8M CMAU in FY23](https://gitlab.com/gitlab-com/Product/-/issues/3650)

## 2022-01-10
1. N - [Kickoff SaaS Free User Program](https://gitlab.com/gitlab-com/free-saas-user-efficiency/-/issues/21)
1. Y - Budget finalization for SaaS Free User Program
1. Y - [CAB Presentations](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/100)
1. Y - [Review Annual Comp Cycle Slates in 1:1s](https://gitlab.com/gitlab-com/Product/-/issues/3659)
1. N - Follow up from Opstrace acquisition with content - [Direction](https://gitlab.com/gitlab-com/corporate-development/-/issues/27), [Community](https://gitlab.com/gitlab-com/corporate-development/-/issues/28)
1. N - [Propose an Engage/Feedback Stage](https://gitlab.com/gitlab-com/Product/-/issues/3626)
1. N - [Set SMAU Goals to hit 3.8M CMAU in FY23](https://gitlab.com/gitlab-com/Product/-/issues/3650)

## 2022-01-02
Happy New Year! Short week with F&F on Monday 2022-01-03
1. N - [Kickoff SaaS Free User Program](https://gitlab.com/gitlab-com/free-saas-user-efficiency/-/issues/21)
1. Y - Ensure progress on [Cost Saving Rapid Action](https://gitlab.com/groups/gitlab-org/-/epics/7212#note_800232777)
1. N - Follow up from Opstrace acquisition with content - [Direction](https://gitlab.com/gitlab-com/corporate-development/-/issues/27), [Community](https://gitlab.com/gitlab-com/corporate-development/-/issues/28)
1. N - [Propose an Engage/Feedback Stage](https://gitlab.com/gitlab-com/Product/-/issues/3626)

## 2021-12-28
Three day week with FF on Monday and NYE on Friday
1. Y - [SaaS Free User Efficiency Organization](https://gitlab.com/gitlab-com/free-saas-user-efficiency/-/issues/21) - (Create Direction Content, Finance Cost Model Change Management, Rename, Kickoff Prep)
1. Y - Conduct and Followup from [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3569)
1. Y - Make [Argus Content Transparent](https://gitlab.com/gitlab-com/Product/-/issues/3567)
1. X - [CI Competition SKO Content Draft](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/1099)

## 2021-12-20
1. Y - [Prep and Deliver Annual Talent Assessment](https://gitlab.com/gitlab-com/Product/-/issues/3601)
1. Y - Free SaaS User Efficiency [Program Organization](https://gitlab.com/gitlab-com/free-saas-user-efficiency/-/issues/21)
1. N - Make [Argus Content Transparent](https://gitlab.com/gitlab-com/Product/-/issues/3567)
1. Y - Prep for [Ops PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3569)
1. N - [Competitive Roadmap Review](https://gitlab.com/gitlab-com/Product/-/issues/3498) and [CI Competition SKO Content Draft](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/1099)

## 2021-12-13
1. Y - Argus Announcement (Internal and External)
1. Y - [Take over Free User Strategy DRI](https://gitlab.com/gitlab-com/Product/-/issues/3561)
1. Y - [Finalize Direction Update Comms](https://gitlab.com/gitlab-com/Product/-/issues/3436)
1. N - Update [TAM and SAM Numbers](https://gitlab.com/gitlab-com/Product/-/issues/3482)
1. Y - Update [Ops Section PI to SpO Contribution](https://gitlab.com/gitlab-com/Product/-/issues/3476) ahead of Monthly PI Review
1. N - [Competitive Roadmap Review](https://gitlab.com/gitlab-com/Product/-/issues/3498) and [CI Competition SKO Content Draft](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/1099)

## 2021-12-06
PLT Offsite in Denver
1. Y - Argus Onboarding/Welcome
1. Y - Argus internal announcement prep
1. Y - PLT Offsite including [FY23 Planning](https://gitlab.com/gitlab-com/Product/-/issues/3445#ops-section)
1. Y - PostHog Eval
1. Y - [Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/3436) (light this month because of [Letters from the Editor](https://gitlab.com/gitlab-com/Product/-/issues/3445#ops-section))
1. [Reconsider Ops Stage Names](https://gitlab.com/gitlab-com/Product/-/issues/2932)

## 2021-11-30
Short, four day week due to F&F on Monday
1. Y - Argus Closing and Onboarding Prep
1. Y - Ops Section [FY22 Lookback, Planning](https://gitlab.com/gitlab-com/Product/-/issues/3445) and [Investment Theme Expectations](https://gitlab.com/gitlab-com/Product/-/issues/3440)
1. Y - PeopleOps Prep for Product Leadership Offsite
1. Y - [Investigate Concord ](https://gitlab.com/gitlab-com/Product/-/issues/3447)for Walmart Opp
1. Y - Work on Updated [TAM and SAM numbers](https://gitlab.com/gitlab-com/Product/-/issues/3482) from IDC
1. Y - Complete Transition of [Consumption Pricing](https://gitlab.com/gitlab-com/Product/-/issues/3392) and [Product UX OKR ownership](https://gitlab.com/gitlab-com/Product/-/issues/3393)

## 2021-11-22
Short, two-day week due to US Thanksgiving Holiday :turkey:
1. Y - Argus Direction Update Finalization
1. Y - Present and followup from [Ops Section Monthly PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3415)
1. Y - Structure and Request for [FY23 Planning and FY22 Lookback](https://gitlab.com/gitlab-com/Product/-/issues/3445) including [Investment Theme Progress](https://gitlab.com/gitlab-com/Product/-/issues/3440)
1. Y - Review [SuS Score Verbatims](https://gitlab.com/gitlab-com/Product/-/issues/3448) and encourage Team Summary/Takeaways
1. N - Complete Transition of [Consumption Pricing](https://gitlab.com/gitlab-com/Product/-/issues/3392) and [Product UX OKR ownership](https://gitlab.com/gitlab-com/Product/-/issues/3393)
1. Y - [Ops Real-Time Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/3384)

## 2021-11-15
1. Y - Argus FAQ Finalization
1. Y - Annual Performance/Potential Calibration
1. Y - Contribute!
1. Y - [Prep for Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3415)
1. Y - Prep and Delivery [Ops Section content for Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/3362)
1. Y - [Ops Section Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/3305)
1. Y - [Transition Storage Visibility Program](https://gitlab.com/gitlab-com/Product/-/issues/3392)
1. Y - [Organize/Transfer DRIness for Product UX Q4 OKR](https://gitlab.com/gitlab-com/Product/-/issues/3393)
1. Y - [Product Analytics Vision Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/3318)

## 2021-11-08
1. Y - Argus Finalization
1. Y - [Contribute Content](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5556)
1. Y - Annual Performance/Potential Callibration
1. Y - [Finalize SKO Content](https://gitlab.com/gitlab-com/Product/-/issues/3220)
1. Y - Various Analyst Presentations
1. Y - [Ops Section Retro Closing Tasks](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12549)
1. Y - QBRs
1. Y - [Next Step on Product Analytics Vision](https://gitlab.com/gitlab-com/Product/-/issues/3318)

## 2021-11-01
1. Y - Argus Comms Plan Next Steps
1. Y - [Product Analytics Vision](https://gitlab.com/gitlab-com/Product/-/issues/3318)
1. Y - Prep and Delivery [Next Analyst Briefing](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5709)
1. Y - Organize [Pitch Content for SKO](https://gitlab.com/gitlab-com/Product/-/issues/3220)
1. Y - Merge MVC for [Documenting Product Upper Bounds](https://gitlab.com/gitlab-com/Product/-/issues/3259)
1. Y - [Add Over-react on Down / Circumspect on Up to Ops PI Review Intent](https://gitlab.com/gitlab-com/Product/-/issues/3296)
1. Next Steps for [Adding more Cross Stage Future Improvements](https://gitlab.com/gitlab-com/Product/-/issues/3205)
1. Y - Review [Ops Section Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/3250)

## 2021-10-25
1. Argus Comms Plan Next Steps
1. Y - Delivery and followup from [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3246)
1. Y - Delivery and Followup from First Analyst Briefing ([IDC](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5713))
1. Y - Prep for [14.4 Ops Section Retrospective](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12549)
1. N - Merge MVC for [Documenting Product Upper Bounds](https://gitlab.com/gitlab-com/Product/-/issues/3259)
1. N - Next Steps for [Adding more Cross Stage Future Improvements](https://gitlab.com/gitlab-com/Product/-/issues/3205)

## 2021-10-18
Getting to this late this week as I was sick on Monday.
1. Y - Review Argus Comms and Forking Plans
1. Y - Ensure our Package Container Registry Migration is optimzied for providing awareness to high usage users on GitLab.com
1. Y - Finalize [Presentation and Walk Through for Analyst Briefings](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5189)
1. Y - Review [Package Use Case](https://gitlab.com/gitlab-com/Product/-/issues/3247)
1. Y - Prep for [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3246)
1. Y - [JFrog and Harness Competitive Blog Review](https://gitlab.com/gitlab-com/Product/-/issues/3260)
1. Y - Socialize need for [Product Upper Bound Expectations](https://gitlab.com/gitlab-com/Product/-/issues/3259)
1. N - Add [More Cross Stage Future Improvements](https://gitlab.com/gitlab-com/Product/-/issues/3205)

## 2021-10-12
Short week with US Holiday on Monday and F&F Day on Friday
1. Y - Argus Acquisition Next Steps - Focus on Comms
1. [Prep for DevOps Platform Analyst Industry Presentations](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5189)
1. Y - [Draft SKO Content to Pitch](https://gitlab.com/gitlab-com/Product/-/issues/3220)
1. Y - [Add Cross-Stage Future Improvements](https://gitlab.com/gitlab-com/Product/-/issues/3205)
1. Learning Goal - [WAF/Container Security Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/2296)

## 2021-10-04
1. Y - Argus Acqusition Next Steps
1. Y - Cover for [Jackie](https://gitlab.com/gitlab-com/Product/-/issues/3080)
1. Y - Prepare for and [Participate in the CAB](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/63)
1. Y - [Record and Distribute Single-App Demo](https://gitlab.com/gitlab-com/Product/-/issues/3116)
1. Y - [Direction Update](https://gitlab.com/gitlab-com/Product/-/issues/3132)
1. Y - [Prepare and Conduct the 14.3 Retrospective](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12442)

## 2021-09-27
I'm OoO this Thursday and Friday
1. Y - Argus Acquisition Next Steps
1. Y - Open and Prep [14.3 Release Retrospective](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12442)
1. Y - [Propose Sub-Department Retrospectives for 14.4](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12383)
1. Y - [Capture Cross Stage Efforts](https://gitlab.com/gitlab-com/Product/-/issues/2935)
1. Y - [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3113)
1. Y - [Ops Section Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/3038)

## 2021-09-20
1. Y - Argus Acquisition Next Steps
1. Y - [Review and Coach on 360 Feedback](https://gitlab.com/gitlab-com/Product/-/issues/3041) with Team
1. Y - [Prep and Deliver Release Kickoff Content](https://gitlab.com/gitlab-com/Product/-/issues/3075)
1. Y - [Prep for Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/3113)
1. N - [Ops Section Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/3038)
1. N - Rerecord and Share [Single App Demo](https://gitlab.com/gitlab-com/Product/-/issues/3116)
1. Y - Prep [Content for Q3 CAB](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/63)
1. N - Perform a Walk Through Using the [new Demo Project](https://gitlab.com/dev-sec-product-private/demos/wayne-enterprises)

## 2021-09-13
In office Monday and Tuesday but out sick Wednesday-Friday

## 2021-09-07
Note - OoO 2021-09-06 for Labor Day

1. Y - Argus Acquisition Next Steps
1. Y - Product-Wide Demo Prep
1. Y - [Host and Follow Up from 14.2 Retrospective](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12340)
1. Y - [Finalize August Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2992)
1. N - [Ops Section Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/3038)
1. N - [Consolidate Technical Eva terms](https://gitlab.com/gitlab-com/Product/-/issues/3043)
1. N - [Propose Moving to Sub-Department based Retrospectives](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12383)

## 2021-08-30

1. Y - Hiring CI PM
1. Y - [Close out Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/2882)
1. Y - [Deployment Direction Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/2951)
1. Y - Follow Up [Item](https://gitlab.com/gitlab-com/Product/-/issues/2955) from Last Retrospectives
1. Y - [Followup from Quarterly Feedback](https://gitlab.com/gitlab-com/Product/-/issues/2905)
1. Y - [Perform 360 Reviews](https://gitlab.com/gitlab-com/Product/-/issues/3041)
1. Y - [Followup and Prep for 14.2 Retrospective](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12340)

## 2021-08-09 - 2021-08-30
:camping: OoO - https://gitlab.com/gitlab-com/Product/-/issues/2882

## 2021-08-02
1. Y - Hiring CI PM
1. Y - CDF Week
1. Y - Prep [Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/2882)
1. Y - Update [Competitive Info for Harness](https://gitlab.com/gitlab-com/Product/-/issues/2738)
1. Y - [Updates from Ops Section Direction Review](https://gitlab.com/gitlab-com/Product/-/issues/2847)
1. Y - [Monthly Direction Updates and Review](https://gitlab.com/gitlab-com/Product/-/issues/2839)
1. Y - [Plan an Ops Section Team Day](https://gitlab.com/gitlab-com/Product/-/issues/2550)
1. Y - MC the [14.1 Retro](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12164)

## 2021-07-26
1. Y - Hiring CI PM
1. Y - Argus Acquisition Next Steps
1. Y - [Amplify SaaS Reliability](https://gitlab.com/gitlab-com/Product/-/issues/2881)
1. Y - Deployment Direction Next Steps - Complete/Publish [Harness](https://gitlab.com/gitlab-com/Product/-/issues/2738) and [WayPoint](https://gitlab.com/gitlab-com/Product/-/issues/2747)
1. N - [Updates from Ops Section Direction Review](https://gitlab.com/gitlab-com/Product/-/issues/2847)
1. N - [Monthly Direction Updates and Review](https://gitlab.com/gitlab-com/Product/-/issues/2839)
1. N - [Plan an Ops Section Team Day](https://gitlab.com/gitlab-com/Product/-/issues/2550)
1. Y - [Ops Section Validation Track Review](https://gitlab.com/gitlab-com/Product/-/issues/2843)
1. Y - [Follow Up from Pricing Review](https://gitlab.com/gitlab-com/Product/-/issues/2846)
1. Y - Learning Goal - Publish learnings from Harness and WayPoint reviews

## 2021-07-19
1. Y - Hiring!
1. Y - [Leto Acquisition Opportunity](https://docs.google.com/document/d/1FtDlaU7AmstnAXk7AMYbJfsKmQMiYSeuL9lW1m2RN2w/edit?ts=60dc17e6) Next Steps
1. Y - [Ops Section Investment Case Review]((https://gitlab.com/gitlab-com/Product/-/issues/2767))
1. Y - [Deployment Direction](https://about.gitlab.com/direction/deployment/) Next Steps
1. Y - [Q3 OKR Prep](https://gitlab.com/gitlab-com/Product/-/issues/2715)
1. Y - [Ops Section Direction Review FollowUp](https://gitlab.com/gitlab-com/Product/-/issues/2847)
1. X - (Meeting Cancelled) Prep for [PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2829)
1. Y - [Kickoff Prep](https://gitlab.com/gitlab-com/Product/-/issues/2791#note_630068113)

## 2021-07-12
F&F on Friday!

1. Y - Hiring!
1. Y - [Argus Acquisition](https://docs.google.com/document/d/1zriE2OX7rjxVSNq1bY5PpdktYHLBFfnWYgHHVbCSHsY/edit) Next Steps
1. Y - [WayPoint](https://gitlab.com/gitlab-com/Product/-/issues/2747) and [Harness](https://gitlab.com/gitlab-com/Product/-/issues/2738) competitive review next steps
1. Y - [Deployment Direction](https://gitlab.com/gitlab-com/Product/-/issues/2668) Next Steps
1. Y - [Consider section-wide Tiering reviews](https://gitlab.com/gitlab-com/Product/-/issues/2731)
1. N - Monthly [Ops Section Investment Case Review](https://gitlab.com/gitlab-com/Product/-/issues/2767)
1. Y - [Prep for Key Review](https://gitlab.com/gitlab-com/Product/-/issues/2780)

## 2021-07-06
Short week with Monday off.

1. Y - [Argus Acquisition](https://docs.google.com/document/d/1zriE2OX7rjxVSNq1bY5PpdktYHLBFfnWYgHHVbCSHsY/edit) Next Steps
1. Y - [Anax Acquisition](https://docs.google.com/document/d/1Oya7wqN_8LzGS4LqsYykB87VVsszWeHsN3PP7dXbOEg/edit) Next Steps
1. Y - [Deployment Direction AMA](https://gitlab.com/gitlab-com/Product/-/issues/2668) Follow Up
1. Competitive Review of N - [WayPoint](https://gitlab.com/gitlab-com/Product/-/issues/2747) and Y - [Harness](https://gitlab.com/gitlab-com/Product/-/issues/2738)
1. Tiering Content Review from Y - [Monthly Direciton Updates](https://gitlab.com/gitlab-com/Product/-/issues/2722) and [consider section-wide reviews](https://gitlab.com/gitlab-com/Product/-/issues/2731)
1. Work on [Ops Section Direction Visualization](https://gitlab.com/gitlab-com/Product/-/issues/2622)

## 2021-06-28
OoO

## 2021-06-21
1. Y - [Argus Acquisition](https://docs.google.com/document/d/1zriE2OX7rjxVSNq1bY5PpdktYHLBFfnWYgHHVbCSHsY/edit) Next Steps
1. Y - [Anax Acquisition](https://docs.google.com/document/d/1Oya7wqN_8LzGS4LqsYykB87VVsszWeHsN3PP7dXbOEg/edit) Next Steps
1. Y - [Deployment Direction AMA](https://gitlab.com/gitlab-com/Product/-/issues/2668)
1. Y - [Prep for Ops PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2689) and followup
1. Y - [Prep for and Deliver 14.1 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/2661)
1. Y - [Complete Coverage Issue for PTO Next Week](https://gitlab.com/gitlab-com/Product/-/issues/2734)
1. N - Work on [Ops Section Direction Visualization](https://gitlab.com/gitlab-com/Product/-/issues/2622)
1. Y - Provide [Dogfooding Input](https://gitlab.com/gitlab-com/Product/-/issues/2717)

## 2021-06-14
1. Y - [Argus Acquisition](https://docs.google.com/document/d/1zriE2OX7rjxVSNq1bY5PpdktYHLBFfnWYgHHVbCSHsY/edit)  Next Steps
1. Y - [Deployment Direction AMA](https://gitlab.com/gitlab-com/Product/-/issues/2668)
1. Y - [Prep for Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/2661)
1. N - [Prep for Ops PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2689)
1. Y - Prioritize Ops Investment Cases
1. N - Work on [Ops Section Direction Visualization](https://gitlab.com/gitlab-com/Product/-/issues/2622)
1. N - Learning Goals - Do a Walk Through for [Deployment Safety](https://docs.gitlab.com/ee/ci/environments/deployment_safety.html)


## 2021-06-07
1. Y - [Cover for Jackie while she is OoO](https://gitlab.com/gitlab-com/Product/-/issues/2627)
1. Y - Sourcing and Screening for PM Positions
1. Y - Finalize Updates to [Deployment Direction](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/82898)
1. Y - [Argus Acquisition](https://docs.google.com/document/d/1zriE2OX7rjxVSNq1bY5PpdktYHLBFfnWYgHHVbCSHsY/edit) investigation and analysis
1. Y - Determine and finalize [DRI](https://gitlab.com/gitlab-com/Product/-/issues/2580) and go-forward plan for CI Abuse
1. Y - Ops Section [Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2585)
1. N - [Review and Prioritize Ops Section Investment Cases](https://gitlab.com/gitlab-com/Product/-/issues/2664)
1. N - Learning Goals - Do a Walk Through for [Deployment Safety](https://docs.gitlab.com/ee/ci/environments/deployment_safety.html)

## 2021-06-01
OoO on an Extended Memorial Day Holiday

## 2021-05-24
1. Y - Sourcing and [Screening](https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/issues/724) for General PM Positions
1. Y - Present [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2574), Follow Up
1. Y - Panel Discussion at [DigitalBritain](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/2866)
1. Y - Include Devs in [Vision Video](https://gitlab.com/gitlab-com/Product/-/issues/2596) more Prominently
1. Y - Share [Gainsight Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/2597) with Ops PM Team
1. Two types of docs cleanup ([Kubernetes](https://gitlab.com/gitlab-com/Product/-/issues/2591) and [Fulfillment](https://gitlab.com/gitlab-com/Product/-/issues/2605))

## 2021-05-17
1. Y - Sourcing and reach out for open PM positions
1. Y - [Prepare and present Kickoff call](https://gitlab.com/gitlab-com/Product/-/issues/2521)
1. Y - Prep Steps for [Cleanup Docs on Deprecated K8s Functionality](https://gitlab.com/gitlab-com/Product/-/issues/2591)
1. Y - [Prep for Ops PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2574)
1. Y - Present AMA at [DevOps Enterprise Summer Europe](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/3040)
1. Y - Adjust and Merge [adding On-Call Schedule Management](https://gitlab.com/gitlab-com/Product/-/issues/2427)

## 2021-05-10
1. Y - Complete Transition of 1:1s in Create Team
1. Y - [Contribute to Roll Out Plan - GitLab Internal Issue](https://gitlab.com/gitlab-com/Product/-/issues/2499)
1. Y - [Project NY Yankees Next Steps - GitLab Internal Issue](https://gitlab.com/gitlab-com/Product/-/issues/2475)
1. Y - [Review and Summarize Monthly Direciton Updates](https://gitlab.com/gitlab-com/Product/-/issues/2440)
1. Y - [Add JiHu Product Content to Handbook](https://gitlab.com/gitlab-com/Product/-/issues/2449)
1. Y - [Finalize Competitive Roadmap Review](https://gitlab.com/gitlab-com/Product/-/issues/2504)
1. Y - [Complete Prep for Product Key Meeting](https://gitlab.com/gitlab-com/Product/-/issues/2503)

## 2021-05-03
With the start of a new month and quarter I adjusted my priorities to focus on Hiring and have been delegating and closing lingering issues I didn't have the bandwidth to prioritize.
1. Y - [Project NY Yankees Next Steps](https://gitlab.com/gitlab-com/Product/-/issues/2475) - [Ops Section Specific Issue](https://gitlab.com/gitlab-com/Product/-/issues/2424)
1. N - [Review and Summarize Monthly Direciton Updates](https://gitlab.com/gitlab-com/Product/-/issues/2440)
1. N - [Add JiHu Product Content to Handbook](https://gitlab.com/gitlab-com/Product/-/issues/2449)
1. Y - [Follow up from Operationizing Ops Section](https://gitlab.com/gitlab-com/Product/-/issues/2410)

## 2021-04-26
I'll be transitioning interim management for Create Stage PMs to their new manager this week.

1. Y - [Deliver and Followup from Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2421)
1. Y - [Complete Monthly Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2440)
1. Y - [Next Steps on Operationalizing the Ops Section](https://gitlab.com/gitlab-com/Product/-/issues/2410)
1. Y - [Next Steps on Bending the Growth Curve](https://gitlab.com/gitlab-com/Product/-/issues/2424)
1. Y - [Finalize Decision on SAST and Code Quality category](https://gitlab.com/gitlab-com/Product/-/issues/2426)
1. Y - [MR to Add On Call Schedule Management as a Category](https://gitlab.com/gitlab-com/Product/-/issues/2427)

## 2021-04-19
1. Y - [Follow Up from Operationalizing the Ops Section](https://gitlab.com/gitlab-com/Product/-/issues/2410)
1. N - [Define Product Leader Results/Success Metrics](https://gitlab.com/gitlab-com/Product/-/issues/2346)
1. Y - [13.12 Kickoff Call](https://gitlab.com/gitlab-com/Product/-/issues/2383)
1. Y - [Review Opportunities for Bending the Growth Curve](https://gitlab.com/gitlab-com/Product/-/issues/2424)
1. Y - [Prior to Monthly Direction Updates, highlight improvements that connect team members to goal/mission](https://gitlab.com/gitlab-com/Product/-/issues/2302)
1. N - [Add On Call Schedule Management as a Category](https://gitlab.com/gitlab-com/Product/-/issues/2427)
1. Y - [Prep for Monthly PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2421)

## 2021-04-12
1. N - [Follow Up from Operationalizing the Ops Section](https://gitlab.com/gitlab-com/Product/-/issues/2410)
1. Y - [Improve GMP CDF Content across all tracks](https://gitlab.com/gitlab-com/Product/-/issues/2345)
1. N - [Define Product Leader Results/Success Metrics](https://gitlab.com/gitlab-com/Product/-/issues/2346)

## 2021-04-05

1. Y - Move forward on [Operationalizing the Ops Section](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78952)
1. N - [Improve GMP CDF Content across all tracks](https://gitlab.com/gitlab-com/Product/-/issues/2345)
1. N - [Define Product Leader Results/Success Metrics](https://gitlab.com/gitlab-com/Product/-/issues/2346)
1. N - Next Steps on [Defining Cross-Functional Career Development](https://gitlab.com/gitlab-com/Product/-/issues/2233)
1. N - Complete the followup to [increase funnel understanding](https://gitlab.com/gitlab-com/Product/-/issues/2365) for our Ops Section PI Review
1. Y - Review and cover highlights from this month's [Ops Section Direction updates](https://gitlab.com/gitlab-com/Product/-/issues/2317)
1. N - Learning Goal - [Pipeline Hierarchy](https://gitlab.com/gitlab-com/Product/-/issues/2308)

## 2021-03-29

I'm putting a focus on CDF content and Performance Indicators for GMPs this week.

1. N -[Improve GMP CDF Content across all tracks](https://gitlab.com/gitlab-com/Product/-/issues/2345)
1. N - [Define Product Leader Results/Success Metrics](https://gitlab.com/gitlab-com/Product/-/issues/2346)
1. Y - Direction updates including [monthly updates](https://gitlab.com/gitlab-com/Product/-/issues/2317), turning it [into a pitch deck with slides](https://gitlab.com/gitlab-com/Product/-/issues/2136), and [focusing on team member connection to goal and mission](https://gitlab.com/gitlab-com/Product/-/issues/2302)
1. Y - Deliver and follow up on [Ops Section PI review](https://gitlab.com/gitlab-com/Product/-/issues/2225)
1. Y - Make progress on [increasing awareness of in-app messaing options amongst ops section PMs](https://gitlab.com/gitlab-com/Product/-/issues/2342)
1. Y - [Kubernetes Agent Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/2190)
1. N - Learning Goal - [Pipeline Hierarchy](https://gitlab.com/gitlab-com/Product/-/issues/2308)

## 2021-03-22
Happy Release Day :rocket: and Spring :sunflower:!

1. Y - [Cover for Kevin while he's on PTO](https://gitlab.com/gitlab-com/Product/-/issues/2292)
1. Y - [Prepare for and present Channel TechChat content for the Ops Section](https://gitlab.com/gitlab-com/channel/channels/-/issues/540)
1. Y - Prepare for and Provide Jackie's CDF and Mark's Career Development Coaching Conversations
1. N - Direction updates including [monthly updates](https://gitlab.com/gitlab-com/Product/-/issues/2317), turning it [into a pitch deck with slides](https://gitlab.com/gitlab-com/Product/-/issues/2136), and [focusing on team member connection to goal and mission](https://gitlab.com/gitlab-com/Product/-/issues/2302).
1. Y - [Prep for Ops PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2225) next Tuesday
1. Y - Progress [R&D Combined Retrospectives](https://gitlab.com/gitlab-com/Product/-/issues/2111) by adding PIs to Async Templates
1. Progress [using BambooHR for Product Groups](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-group-engineering/-/issues/240) via Handbook Updates
1. N - Learning Goal - [Pipeline Hierarchy](https://gitlab.com/gitlab-com/Product/-/issues/2308)

## 2021-03-15
My [board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston) is back! I'm going to be putting some emphasis on my #1 priority, Team Development, this week.

1. Y - [Clarify Reponsibilities between GMP and Director of Product](https://gitlab.com/gitlab-com/Product/-/issues/2279)
1. Y - [Update CDF for GMPs](https://gitlab.com/gitlab-com/Product/-/issues/2301)
1. Y - [Ops Section Cross Functional Career Devleopment](https://gitlab.com/gitlab-com/Product/-/issues/2233)
1. Y - [Prep for 13.11 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/2253)
1. Y - [Document Investment Process for Sections](https://gitlab.com/gitlab-com/Product/-/issues/2230)
1. Y - Next Steps to [Clarify R&D Wide Retrospectives](https://gitlab.com/gitlab-com/Product/-/issues/2111)
1. Y - Next Steps to [Move to BambooHR as SSOT for Specialty](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-group-engineering/-/issues/240)
1. Y - Learning Goal - Working with Kevin on the [Deployment direction](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/76519)

## 2021-03-08
Happy International Women's Day!

1. Y - [Complete Monthly Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2186)
1. N - [Convert Direction into Pitch Deck](https://gitlab.com/gitlab-com/Product/-/issues/2136)
1. Y - [Follow Up Updates for Specialty Field Addition](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-group-engineering/-/issues/240)
1. Y - [Distribute Insights from QBRs](https://gitlab.com/gitlab-com/Product/-/issues/2089)
1. Y - [Document how to Consider External Opportunities as a GitLab PM](https://gitlab.com/gitlab-com/Product/-/issues/2240)
1. Y - [Progress on Product Finance Model](https://gitlab.com/gitlab-com/Product/-/issues/2152)
1. Y - [Encourage More Engagement from all R&D Functions in Retrospectives](https://gitlab.com/gitlab-com/Product/-/issues/2111)
1. Y - Learning Goal - [Review other Product Orgs Investment Process](https://gitlab.com/gitlab-com/Product/-/issues/2231)

## 2021-03-01
Happy March :four_leaf_clover:!
1. Y - [Propose Summary Insights from QBRs](https://gitlab.com/gitlab-com/Product/-/issues/2089)
1. N - [Complete Monthly Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2186)
1. N - [Follow Up Updates for Specialty Field Addition](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-group-engineering/-/issues/240)
1. N - [Convert Direction into Pitch Deck](https://gitlab.com/gitlab-com/Product/-/issues/2136)
1. Y - [Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/2223) 
1. Y - [Progress on Product Finance Model](https://gitlab.com/gitlab-com/Product/-/issues/2152)
1. Y - [Clarify where TAM Means Total Addressable Market](https://gitlab.com/gitlab-com/Product/-/issues/2174)
1. N - Learning Goal - [Review other Product Orgs Investment Process](https://gitlab.com/gitlab-com/Product/-/issues/2231)

## 2021-02-22
It's Release Day :rocket:, and I'm [OoO Tuesday-Thursday](https://gitlab.com/gitlab-com/Product/-/issues/2196) with F&F Day on Friday. 
1. Y - [Communicate Change of Speciality SSSOT](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-group-engineering/-/issues/240)
1. Y - Interviews for the GMP, Create position on Monday

## 2021-02-15
Short week because of US President's Day Holiday. Also :scream: - [My Board Isn't Working](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston)! Update - It works now. :smile:
1. N - [Communicate Change of Speciality SSSOT](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-group-engineering/-/issues/240)
1. Y - [Coordinate and Delegate Vision for Pipeline/Jobs](https://gitlab.com/gitlab-com/Product/-/issues/2116)
1. N - [Script for GitLab CI/CD Introduction](https://gitlab.com/gitlab-com/Product/-/issues/1978)
1. N - [Make Progress on Product Financial Model](https://gitlab.com/gitlab-com/Product/-/issues/2152)
1. N - [Turn the Ops Direction Page into a Pitch Deck](https://gitlab.com/gitlab-com/Product/-/issues/2136)
1. Y - Learning Goal - [Gitaly and Infrastructure/Cost Challenges](https://gitlab.com/gitlab-com/Product/-/issues/2149)

## 2021-02-08
1. Y - [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/2073)
1. Y - Support the Opening of [Dev::Create GMP Role and Interview Process](https://gitlab.com/gitlab-com/Product/-/issues/2127)
1. Y - Reach out (or delegate) to our [Billionth Build User](https://gitlab.com/gitlab-com/Product/-/issues/2087)
1. Y - [Follow Up from Ops Section Offsite](https://gitlab.com/gitlab-com/Product/-/issues/1959) via [Monthly Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2005)
1. Y - Propose a plan for [Encourage more Product Engagement in R&D Retros](https://gitlab.com/gitlab-com/Product/-/issues/2111)
1. N - [Make Progress on Guided CI/CD Introduction](https://gitlab.com/gitlab-com/Product/-/issues/1978) content
1. Y - [Create a Window in Problem and Solution Validation](https://gitlab.com/gitlab-com/Product/-/issues/1941)
1. Y - [Complete Competitor Roadmap Review](https://gitlab.com/gitlab-com/Product/-/issues/2074)
1. Y - [MVC and New Config Review](https://gitlab.com/gitlab-com/Product/-/issues/2121)
1. N - Learning Goal - [Azure DevOps Pipelines](https://www.udemy.com/course/azure-devops-ci-cd-pipelines/)

## 2021-02-01
:tada: Start of a new Fiscal Year!
1. Y - Attend [Sales QBRs](https://gitlab.com/gitlab-com/Product/-/issues/2089) on Wed, Thur and Friday
1. N - [Follow Up from Ops Section Offsite](https://gitlab.com/gitlab-com/Product/-/issues/1959) via [Monthly Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2005)
1. N - [Make Progress on Guided CI/CD Introduction](https://gitlab.com/gitlab-com/Product/-/issues/1978) content
1. N - [Create a Window in Problem and Solution Validation](https://gitlab.com/gitlab-com/Product/-/issues/1941)
1. N - [Reach out to Billionth Pipeline User](https://gitlab.com/gitlab-com/Product/-/issues/2087)
1. Y - [Improve Source-a-thon Scheduling for better geographic distribution in hiring](https://gitlab.com/gitlab-com/Product/-/issues/2081)

## 2021-01-25
1. Y - Attend and Participate in the Product [Crucial Conversations Training](https://gitlab.com/gitlab-com/Product/-/issues/1810)
1. Y - Complete [Direction Page changes as part of our Tiering change announcement](https://gitlab.com/gitlab-com/packaging-and-pricing/pricing-handbook/-/issues/324)
1. N - [Follow Up from Ops Section Offsite](https://gitlab.com/gitlab-com/Product/-/issues/1959) via [Monthly Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2005)
1. Y - [Ensure PMs are Providing a Compelling Vision and Goal](https://gitlab.com/gitlab-com/Product/-/issues/2025) to Product Groups
1. Y - [Remind PMs to Consider Discertionary Bonus for Product Group Members](https://gitlab.com/gitlab-com/Product/-/issues/2024)
1. N - [Create a Window in Problem and Solution Validation](https://gitlab.com/gitlab-com/Product/-/issues/1941)
1. Learning Goal - Weekly Walk Through! 

## 2020-01-19
Shorter week with MLKJ Day on Monday
1. Y - [Kickoff Prep and Delivery](https://gitlab.com/gitlab-com/Product/-/issues/1944)
1. Y - [Provide Ops Section Tier Content for SKO](https://gitlab.com/gitlab-com/Product/-/issues/2017)
1. Y - Ensure we have strong next steps for [Auto DevOps Adoption](https://gitlab.com/gitlab-com/Product/-/issues/1801)
1. Y - [Publish Blog Post for Completing the DevOps Loop](https://gitlab.com/gitlab-com/Product/-/issues/1977)
1. N - [Follow Up from Ops Section Offsite](https://gitlab.com/gitlab-com/Product/-/issues/1959) via [Monthly Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2005)
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/2009) now with insights on Tech Evals!
1. Y - [Finalize and Close Out GitHub Roadmap Review](https://gitlab.com/gitlab-com/Product/-/issues/1846)
1. Y - Learning Goal - Attend [Crucial Conversation Training](https://gitlab.com/gitlab-com/Product/-/issues/1810)

## 2020-01-11
Shorter week with Friends and Family Day on Friday
1. Y - Solidify Next Steps for [Auto DevOps Adoption](https://gitlab.com/gitlab-com/Product/-/issues/1801)
1. Y - Participate in and [Follow Up from Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1929)
1. Y - [Team Compensation Review](https://gitlab.com/gitlab-com/Product/-/issues/1961)
1. Y - [Draft and Prepare to Publish blog post on Completing the Feedback Loop](https://gitlab.com/gitlab-com/Product/-/issues/1977)
1. Y - [Monthly MVC, New Config and Pricing Review](https://gitlab.com/gitlab-com/Product/-/issues/1963)
1. N - [Follow Up from Ops Section Offsite](https://gitlab.com/gitlab-com/Product/-/issues/1959)
1. Y - [13.9 Release Kickoff Prep](https://gitlab.com/gitlab-com/Product/-/issues/1944)
1. N - [Finalize and Close Out GitHub Roadmap Review](https://gitlab.com/gitlab-com/Product/-/issues/1846)
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1976)

## 2020-01-04
Happy New Year :tada:! Limited availability with a Product Leadership Offsite this week.
1. Y - Participate in Product Leadership Offsite
1. Y - [Prep for Ops Section PI Update](https://gitlab.com/gitlab-com/Product/-/issues/1929)
1. N - Progress [Auto DevOps Adoption](https://gitlab.com/gitlab-com/Product/-/issues/1801) issue
1. Y - Track [GitHub Public Roadmap Review Activities](https://gitlab.com/gitlab-com/Product/-/issues/1846)
1. Y - [Add Product Group Presentation to Ops Section Page](https://gitlab.com/gitlab-com/Product/-/issues/1676)
1. Y - [Create a weekly walk through issue template automation](https://gitlab.com/gitlab-com/Product/-/issues/1934)
1. Y - Add a [Communicating to Internal Executives Competency](https://gitlab.com/gitlab-com/Product/-/issues/1259)
1. Learning Goal - None this week due to to PLT Offsite

## 2020-12-21
Short week, on PTO W-F.
1. Y - [Contribute Ops Content for SKO Product Key Note](https://gitlab.com/gitlab-com/Product/-/issues/1856)
1. Y - [Merge Updated Auto DevOps Vision](https://gitlab.com/gitlab-com/Product/-/issues/1843)
1. Y - [Distill the lessons learned from IaC Initial Success](https://gitlab.com/gitlab-com/Product/-/issues/1858)
1. Y - [Ops Section PI Exploration](https://gitlab.com/gitlab-com/Product/-/issues/1857)
1. Y - Learning Goal - [Walk Through GitLab CI - Focus on what things aren't named](https://gitlab.com/gitlab-com/Product/-/issues/1848)

## 2020-12-14
Shorter week with Friends and Family Day on Friday.
1. Y - [Ops Section PM - Annual Performance Review Conversations](https://gitlab.com/gitlab-com/Product/-/issues/1851)
1. Y - [Update Auto DevOps Vision](https://gitlab.com/gitlab-com/Product/-/issues/1843)
1. Y - [Prep and Delivery 13.8 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1826)
1. Y - [Review GitHub Public Roadmap](https://gitlab.com/gitlab-com/Product/-/issues/1846)
1. Y - [Weekly Commit Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1850)
1. Y - Learning Goal - [Package Data Audit for items to display in UI](https://gitlab.com/gitlab-org/gitlab/-/issues/260426)

## 2020-12-07
1. Y - Prep for [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1799)
1. Y - Make Progress on organizing for [Auto DevOps Adoption](https://gitlab.com/gitlab-com/Product/-/issues/1801)
1. Y - Update `What's New` to the description and files and Merge the [TAM/SAM Fix](https://gitlab.com/gitlab-com/Product/-/issues/1787)
1. Y - [Finalize and Present Sensing Mechanism Survey Results](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - [Improve our use of the word `land` in our Product Direction](https://gitlab.com/gitlab-com/Product/-/issues/1818)
1. N - [Communicate Future Investment Plan by Category in Groups](https://gitlab.com/gitlab-com/Product/-/issues/1708)
1. N - [Add Product Group displays to Ops Section Page](https://gitlab.com/gitlab-com/Product/-/issues/1676)
1. Y - Learning Goal - Create my own [Ops Section Chart in Sisense](https://gitlab.com/gitlab-com/Product/-/issues/1793)

## 2020-11-30
1. Y - [Coverage for Kevin while he's CEO Shadowing](https://gitlab.com/gitlab-com/Product/-/issues/1742)
1. Y - [Fix TAM/SAM Calculations Using New Functional Market Breakout](https://gitlab.com/gitlab-com/Product/-/issues/1787)
1. Y - [Review Direction and other Monthly Updates](https://gitlab.com/gitlab-com/Product/-/issues/1751)
1. N - [Finalize and Present Sensing Mechanism Survey Results](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - [Consider Group Direction Pages (instead of Category)](https://gitlab.com/gitlab-com/Product/-/issues/1792)
1. N - [Communicate Future Investment Plan by Category in Groups](https://gitlab.com/gitlab-com/Product/-/issues/1708)
1. N - [Add Product Group displays to Ops Section Page](https://gitlab.com/gitlab-com/Product/-/issues/1676)
1. N - [Ensure BambooHR and Team.yml are in sync](https://gitlab.com/gitlab-com/Product/-/issues/1772)
1. Learning Goal - Create my own [Ops Section Chart in Sisense](https://gitlab.com/gitlab-com/Product/-/issues/1793)

## 2020-11-23
Out on Wednesday, Thursday and Friday or this week.
1. N - [Finalize and Present Sensing Mechanism Survey Results](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - [Direction Page Updates](https://gitlab.com/gitlab-com/Product/-/issues/1751)
1. Y - [Add +10 Investment Priorioties to Ops Direction](https://gitlab.com/gitlab-com/Product/-/issues/1771)
1. N - [Communicate Future Investment Plan by Category in Groups](https://gitlab.com/gitlab-com/Product/-/issues/1708)
1. N - [Add Product Group displays to Ops Section Page](https://gitlab.com/gitlab-com/Product/-/issues/1676)
1. Y - [Create a handbook include for displaying recent release post items from a group](https://gitlab.com/gitlab-com/Product/-/issues/1681)
1. Y - [Ensure BambooHR and Team.yml are in sync](https://gitlab.com/gitlab-com/Product/-/issues/1772)

## 2020-11-16
ENT Leadership QBR on Wednesday
1. Y - Contribute to QBRs
1. Y - [Schedule Customer Contact](https://gitlab.com/gitlab-com/Product/-/issues/1740)
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1741)
1. Y - [Kickoff Call on Wednesday](https://gitlab.com/gitlab-com/Product/-/issues/1704)
1. Y - [Complete DI&B Training](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/issues/487)
1. Y - [Ops Section Dashboard Creation](https://gitlab.com/gitlab-com/Product/-/issues/1693)
1. N - [Finalize and Present Sensing Mechanism Survey Results](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - [Present Ops Section Opportunities in PLT](https://gitlab.com/gitlab-com/Product/-/issues/1701)
1. N- Learning Goal - Expirementation, A/B, and Feature Flags market

## 2020-11-09
I'm OoO on Wednesday for Veterans Day and attending QBRs on Monday and Friday
1. Y - Contribute to QBRs
1. Y - Present and Followup from [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1690)
1. Y - [Add Ops Section Opportunities in SSOT](https://gitlab.com/gitlab-com/Product/-/issues/1701)
1. N - [Create an Ops Section PI Dashboard](https://gitlab.com/gitlab-com/Product/-/issues/1701) of my own 
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1718)
1. Y - [Update Product Investment by Group based on new method add new table that includes growth](https://gitlab.com/gitlab-com/Product/-/issues/1712)
1. Y - Learning Goal - Review Actionable Insights from [CI Adoption Research](https://gitlab.com/groups/gitlab-org/-/epics/4124)

## 2020-11-02
I'm OoO on Wednesday and attending QBRs on Monday and Tuesday morning.

1. Y - Contribute to QBRs
1. Y - Begin prep for [Ops Section PI review](https://gitlab.com/gitlab-com/Product/-/issues/1690)
1. Y - [Complete feature assignment for PnP Research](https://gitlab.com/gitlab-com/packaging-and-pricing/pricing-handbook/-/issues/268)
1. Y - Finalize and [merge TAM and SAM update](https://gitlab.com/gitlab-com/Product/-/issues/1425)
1. [Assign Walk Through App Ownership](https://gitlab.com/gitlab-com/Product/-/issues/1652)
1. Y - Widely distribute [Product<>GTM survey](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - Review and respond to [Quarterly Feedback](https://gitlab.com/gitlab-com/Product/-/issues/1651)
1. Y - [Weekly Top Opp Review](https://gitlab.com/gitlab-com/Product/-/issues/1692) (first week of the quarter makes this an odd one)
1. Y - Learning Goal - Read and Contribute Lessons Learned from [Gartner 2020 IT Ops and Cloud Management Reports](https://gitlab.com/gitlab-com/Product/-/issues/1668)

## 2020-10-27
1. Y - Prep for [Ops Section CAB Presentation](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/2543)
1. Y- Manager Handoffs as part of [Jason's Transition](https://gitlab.com/gitlab-com/Product/-/issues/1573)
1. Y - Get final review and [update TAM from IDC Reports](https://gitlab.com/gitlab-com/Product/-/issues/1425)
1. Y - Next Steps on [Product <> GTM Sensing Mechanisms Survey](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Review [Gartner IT Ops and Cloud Management Reports](https://gitlab.com/gitlab-com/Product/-/issues/1668)
1. Y - Competitive [Research on Fyipe](https://gitlab.com/gitlab-com/Product/-/issues/1672)
1. Y - Weekly [Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1671)
1. Y - Next [Steps on Quarterly Feedback](https://gitlab.com/gitlab-com/Product/-/issues/1651)
1. Y - Progress [Q4 Ops Section Analyst Update](https://gitlab.com/gitlab-com/Product/-/issues/1654)
1. Y - Learning Goal - Principles for CI YAML Syntax

## 2020-10-20

1. Y - [Update SAM and TAM](https://gitlab.com/gitlab-com/Product/-/issues/1425) now that we have usable data
1. Y - [Kickoff Delivery](https://gitlab.com/gitlab-com/Product/-/issues/1599)
1. Y - Send out [Sensing Mechanism Survey](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - Create [Product Tier Strategy Review Process](https://gitlab.com/gitlab-com/Product/-/issues/1653)
1. Y - [Ops Direction Update](https://gitlab.com/gitlab-com/Product/-/issues/1645)
1. Y - Solicit Personal [Quaterly Feedback](https://gitlab.com/gitlab-com/Product/-/issues/1651)
1. N - [Assign Walk-Through App Ownership](https://gitlab.com/gitlab-com/Product/-/issues/1652)
1. Y - Begin discussion on [Ops Section Analyst Update](https://gitlab.com/gitlab-com/Product/-/issues/1654)
1. Y (AMA Scheduled) - Learning Goal - Principles for CI YAML Syntax

## 2020-10-12
_Note_: Short week with Columbus Day on Monday

1. Y - [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1588)
1. Y - Hiring - Interviews and Hiring Prep for GMP, Verify position
1. N - [Update SAM and TAM](https://gitlab.com/gitlab-com/Product/-/issues/1425) now that we have usable data
1. Y - [Kickoff Prep](https://gitlab.com/gitlab-com/Product/-/issues/1599)
1. Y - [Product GTM Sensing Mechanisms Survey](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - [Follow Up on 5 Minute Production App](https://gitlab.com/gitlab-com/Product/-/issues/1601)
1. Y - Resolve [Ops Section FY21 - Q4 Goals](https://gitlab.com/gitlab-com/Product/-/issues/1595) Discussion
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1626)
1. Learning Goal - Principles for CI YAML Syntax

## 2020-10-05
_Note_: Friends and Family Day on Friday.

1. Y - [Ops Section PI Review Prep](https://gitlab.com/gitlab-com/Product/-/issues/1588)
1. Y - [5 Minute Production App Walkthrough](https://gitlab.com/gitlab-com/Product/-/issues/1601)
1. N - [Product <> GTM Sensing Mechanisms Survey](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - [Discussion about having Ops Section Q4 goals](https://gitlab.com/gitlab-com/Product/-/issues/1595)
1. Y - [Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1598)
1. Learning Goal - Principles for CI YAML Syntax

## 2020-09-28
I ended up prioritizing communicating and opening the Group Manager Product, Verify position during this week.

1. N - [Build a Micro-services example for walk-throughs](https://gitlab.com/gitlab-com/Product/-/issues/839)
1. N - [Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1549)
1. N - [Product <> GTM Sensing Mechanisms Update](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - [PM Coverage for Orit](https://gitlab.com/gitlab-com/Product/-/issues/1524)
1. Y - [Direction Page Reviews](https://gitlab.com/gitlab-com/Product/-/issues/1542)
1. Y - Learning Goal - Research for [Define a Vision for Ops Executive Dashboard](https://gitlab.com/gitlab-com/Product/-/issues/1518)

## 2020-09-21
Note - OoO on Thursday for Pod Parent day.

1. Y - [Review and Update Ops Section Direction](https://gitlab.com/gitlab-com/Product/-/issues/1542)
1. Y - [Update TAM and SAM](https://gitlab.com/gitlab-com/Product/-/issues/1425) now that we have new IDC data
1. N - [Build a Micro-services example for walk-throughs](https://gitlab.com/gitlab-com/Product/-/issues/839)
1. Y - [PM Coverage for Orit](https://gitlab.com/gitlab-com/Product/-/issues/1524)
1. N - [Product <> GTM Sensing Mechanisms Update](https://gitlab.com/gitlab-com/Product/-/issues/1540)
1. Y - [Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1549)
1. N - Learning Goal - Research for [Define a Vision for Ops Executive Dashboard](https://gitlab.com/gitlab-com/Product/-/issues/1518)

## 2020-09-14
1. Y - [Ops Section Group Conversation](https://gitlab.com/gitlab-com/Product/-/issues/1387) Wednesday
1. Y - [Review Draft Funnels](https://gitlab.com/gitlab-com/Product/-/issues/1386)
1. Y - Prep and Deliver Company-wide [Monthly Release Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1502)
1. Y - Follow up actions from [Notes and Actions from Re-reading Accelerate](https://gitlab.com/gitlab-com/Product/-/issues/1510)
1. Y - [Close out Initial Product Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234) Issue
1. Y - Summarize and transfer next steps in [Are we really dogfooding Feature Flags?](https://gitlab.com/gitlab-com/Product/-/issues/1460)
1. N - Learning Goal - Research for [Define a Vision for Ops Executive Dashboard](https://gitlab.com/gitlab-com/Product/-/issues/1518)

## 2020-09-08
1. Y - This is CDF Review week - and Jason and Kevin and I will have our first CDF Review Review (so we can be better at reviewing CDFs) next week. 
1. Y - Prep for [Ops Section PI Review Meeting](https://gitlab.com/gitlab-com/Product/-/issues/1490) next Monday
1. Y - Prep for [Ops Section Group Conversation](https://gitlab.com/gitlab-com/Product/-/issues/1387) next week Wednesday
1. N - [Review Draft Funnels](https://gitlab.com/gitlab-com/Product/-/issues/1386)
1. N -[Close out Initial Product Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234) issue
1. N - [Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1503)
1. Learning Goal - [Incorporate Notes from re-reading Accelerate](https://gitlab.com/gitlab-com/Product/-/issues/1510)

## 2020-08-17
I'm off the next two weeks so trying to end the week with things wrapped up.

1. Y - [Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1418)
1. Y - [Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1438)
1. Y - Prep [PTO Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/1440)
1. N - [Docs Language Review for Restrictiveness](https://gitlab.com/gitlab-com/Product/-/issues/1410)
1. N - [PI Section Status and Other Updates](https://gitlab.com/gitlab-com/Product/-/issues/1428)
1. N - Setup interviews from new NPS for [100 Customers a Year](https://gitlab.com/gitlab-com/Product/-/issues/1280)
1. Y - Learning Goal - Reading [Accelerate](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations-ebook/dp/B07B9F83WM) again.

## 2020-08-10
Friday is Friends and Family Day!

1. Y - [Prep for Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1418)
1. Y - [MVC and New Config Review](https://gitlab.com/gitlab-com/Product/-/issues/1413)
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1417) (Now with Command and Account Plans! I'm going to [propose an update to the Sales Hanbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8697))
1. Y - Make Progress on [Draft Funnels for All SMAU](https://gitlab.com/gitlab-com/Product/-/issues/1386)
1. N - [Docs Language Review for Restrictiveness](https://gitlab.com/gitlab-com/Product/-/issues/1410)
1. N - Setup interviews from new NPS for [100 Customers a Year](https://gitlab.com/gitlab-com/Product/-/issues/1280)
1. Learning Goal - [Monorepos](https://gitlab.com/gitlab-com/Product/-/issues/1389)

## 2020-08-03
1. Y - Prep, Deliver and Follow Up for [Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1290)
1. Started - [Draft Funnels for All SMAU](https://gitlab.com/gitlab-com/Product/-/issues/1386)
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1356)
1. N - Setup interviews from new NPS for [100 Customers a Year](https://gitlab.com/gitlab-com/Product/-/issues/1280)
1. Y - Ensure progress on [Encouraging iterations smaller than our monthly milestone](https://gitlab.com/gitlab-com/Product/-/issues/1247)
1. N - Close out first round of [Product Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234) and communicate
1. Learning Goal - [Monorepos](https://gitlab.com/gitlab-com/Product/-/issues/1389)

## 2020-07-27
1. Y - [Coverage for Jackie who is OoO](https://gitlab.com/gitlab-com/Product/-/issues/1326)
1. Y - [Ops Section GC and Follow Up](https://gitlab.com/gitlab-com/Product/-/issues/1257)
1. Y - [Prep for Ops Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1290)
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1356)
1. Y - [Define Release Stage PI Structure](https://gitlab.com/gitlab-com/Product/-/issues/1381)
1. Y - Make progress on [Encouraging iterations smaller than our monthly milestone](https://gitlab.com/gitlab-com/Product/-/issues/1247)
1. Y - Make progress on [100 Customers a Year](https://gitlab.com/gitlab-com/Product/-/issues/1280)
1. N - Close out first round of [Product Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234) and communicate
1. Learning Goal - Getting started with GitLab CD 

## 2020-07-20

This week I'm transitioning my schedule to [an earlier start](https://gitlab.com/kencjohnston/README/-/commit/2cdeda2eeee3b1f986ba1350171b28d75dbf638a). As a result I have limited availability.

1. Y - [Monday - Company Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1332)
1. Y - [Prep for Ops GC](https://gitlab.com/gitlab-com/Product/-/issues/1257)
1. Y - [Weekly Top Opportunities Review](https://gitlab.com/gitlab-com/Product/-/issues/1356)
1. Y - Make progress on [Encouraging iterations smaller than our monthly milestone](https://gitlab.com/gitlab-com/Product/-/issues/1247)
1. N - Make progress on [100 Customers a Year](https://gitlab.com/gitlab-com/Product/-/issues/1280)
1. Y - Transfer [Collecting Telemetry Arch issues block MAU metrics](https://gitlab.com/gitlab-com/Product/-/issues/1316) to telemetry team
1. N - Close out first round of [Product Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234) and communicate

## 2020-07-13
1. Y - [Ops Section Tiering Review](https://gitlab.com/gitlab-com/Product/-/issues/1324)
1. Y - [Weekly Top Opportunity Review](https://gitlab.com/gitlab-com/Product/-/issues/1323)
1. Y - [Finalize Ops Section PM Org Structure adjustments](https://gitlab.com/gitlab-com/Product/-/issues/1319)
1. Y - Make Progress on [100 Customers a Year](https://gitlab.com/gitlab-com/Product/-/issues/1280)
1. Y - Make Progress on [Collecting Telemetry Arch issues for Q3 OKR](https://gitlab.com/gitlab-com/Product/-/issues/1316)
1. Y - [Merge first round of Product Group Functional Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234)
1. Y - [Kickoff Prep]()
1. Learning Goal - MLOps ([CML Walk-through](https://gitlab.com/gitlab-com/Product/-/issues/1318))

## 2020-07-06
This was a short week for me and I never got to creating my priorities list

## 2020-06-29
Short week as I'm out Friday for observance of the US July 4th Holiday.

1. Y - [Present and Follow Up from Performance Indicator Review](https://gitlab.com/gitlab-com/Product/-/issues/1165)
1. Y - Finalize Process for [understanding Top 5 Inflight Opportunities](https://gitlab.com/gitlab-com/Product/-/issues/1261)
1. Y - Follow up from [Attending DevOps Enterprise Summit](https://gitlab.com/gitlab-com/Product/-/issues/1262)
1. Y - Make Progress on [Build out Product Group Function Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234)
1. N - Make Progress on [Encourage Iterations Smaller than our Monthly Milestones](https://gitlab.com/gitlab-com/Product/-/issues/1247)
1. N - Begin work on connecting with [100 Customers a Year](https://gitlab.com/gitlab-com/Product/-/issues/1280)
1. Y - Begin work on [Understanding Tier Value by Stage](https://gitlab.com/gitlab-com/Product/-/issues/1276)
1. Y (Completed) Learning Goal - Reading [Competing Against Luck](https://www.amazon.com/gp/product/0062435612/)

## 2020-06-22
I'll be attending [virtual DOES](https://events.itrevolution.com/virtual/) this week Tue-Thur at odd times so my calendar will be adjusted.

1. Y - [Attend DevOps Enterprise Summit London](https://gitlab.com/gitlab-com/Product/-/issues/1262)
1. Y - [Prepare for Junes NSM Review](https://gitlab.com/gitlab-com/Product/-/issues/1165)
1. N - [Perform One Walk-Through](https://gitlab.com/gitlab-com/Product/-/issues/951)
1. Y - [Understand Top 5 Inflight Opportunties](https://gitlab.com/gitlab-com/Product/-/issues/1261)
1. Y - Make Progress on [Build out Product Group Function Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234)
1. N - Make Progress on [Encourage Iterations Smaller than our Monthly Milestones](https://gitlab.com/gitlab-com/Product/-/issues/1247)
1. Learning Goal - Reading [Competing Against Luck](https://www.amazon.com/gp/product/0062435612/)

## 2020-06-15
1. Y - [Prepare for and Deliver 13.2 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1227)
1. Y - [Prepare for and deliver Ops Section June GC](https://gitlab.com/gitlab-com/Product/-/issues/1240)
1. N - [Prepare for Junes NSM Review](https://gitlab.com/gitlab-com/Product/-/issues/1165)
1. In Review - [Build out Product Group Function Competencies](https://gitlab.com/gitlab-com/Product/-/issues/1234)
1. In Review - [Complete Understanding of Product Hierarchy and Org Structure Distinction](https://gitlab.com/gitlab-com/Product/-/issues/1226)
1. Learning Goal - Reading [Competing Against Luck](https://www.amazon.com/gp/product/0062435612/)

## 2020-06-08
Short week, Friends and Family day on Friday

1. Y - [Ops Section - Talent Assessment](https://gitlab.com/gitlab-com/Product/-/issues/1223)
1. Y - [Document that for Certain Ops Sections Groups they are focused on SMAPU/GMAPU versus SMAU/GMAU](https://gitlab.com/gitlab-com/Product/-/issues/1219)
1. Y - [Finalize Team Member Realignment to Verify and Fulfillment](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7795)
1. In Review - [Add Standard Steps to Jenkins Migration Process Documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/220790)
1. Y - [Prep for June NSM Review](https://gitlab.com/gitlab-com/Product/-/issues/1161)
1. Y - [Verify - Consider prioritizing UX polish higher in CI](https://gitlab.com/gitlab-com/Product/-/issues/1209)
1. Learning Goal - Reading [Competing Against Luck](https://www.amazon.com/gp/product/0062435612/)

## 2020-06-01
1. Y - [Update Ops Section Themes and Record Video](https://gitlab.com/gitlab-com/Product/-/issues/994) for E-Group Review
1. Y - [Prep for June NSM Review](https://gitlab.com/gitlab-com/Product/-/issues/1161)
1. Y - [Coordinate Documentation of all Ops Section Usage Ping Measures](https://gitlab.com/gitlab-com/Product/-/issues/1166)
1. N - [Verify - Consider prioritizing UX polish higher in CI](https://gitlab.com/gitlab-com/Product/-/issues/1209)
1. N - [Product Walk-through - Add New CI Job based on container built in different project](https://gitlab.com/gitlab-com/Product/-/issues/1198)
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. Y - Learning Goal - Reading [Competing Against Luck](https://www.amazon.com/gp/product/0062435612/) 

## 2020-05-25
PTO

## 2020-05-18
1. Y - [Kickoff Prep](https://gitlab.com/gitlab-com/Product/-/issues/1156)
1. Y - Close down the CI Minutes [Efficiency initiative](https://gitlab.com/gitlab-org/gitlab/-/issues/210457)
1. N - [Update Ops Section Themes and Record Video](https://gitlab.com/gitlab-com/Product/-/issues/994) for E-Group Review
1. N - Follow Up and Goal Setting from [NSM Review](https://gitlab.com/gitlab-com/Product/-/issues/1161)
1. N - [Coordinate Documentation of all Ops Section Usage Ping Measures](https://gitlab.com/gitlab-com/Product/-/issues/1166)
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. N - Learning Goal - Reading [Competing Against Luck](https://www.amazon.com/gp/product/0062435612/) 

## 2020-05-11
1. Y - Come to a decision on timeline for rollout of this [Efficiency initiative](https://gitlab.com/gitlab-org/gitlab/-/issues/210457)
1. Y - Finalize [Ops Section North Star Metric Review Follow Up](https://gitlab.com/gitlab-com/Product/-/issues/1049)
1. Y - [Kickoff Prep](https://gitlab.com/gitlab-com/Product/-/issues/1156)
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. Y - Follow Up and Goal Setting from [NSM Review](https://gitlab.com/gitlab-com/Product/-/issues/1161)
1. Y - Learning Goal - [Multi-Project Pipelines](https://gitlab.com/groups/gitlab-org/-/epics/3156#note_340245113) - [YouTube Video](https://www.youtube.com/watch?v=fTqUbXbUBcA)

## 2020-05-04
1. Y - Finalize [Ops Section GC Prep](https://gitlab.com/gitlab-com/Product/-/issues/1108)
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. Y - [Ops Section North Star Metric Review Follow Up](https://gitlab.com/gitlab-com/Product/-/issues/1049)
1. Y - [Ensure Regular CDF Reviews for Ops Section PMs](https://gitlab.com/gitlab-com/Product/-/issues/1136)
1. Y - [Transition Stable Counterparts for Configure and Monitor](https://gitlab.com/gitlab-com/Product/-/issues/1135)
1. Y - Learning Goal - [Advanced Feature Flag Usage](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) 

## 2020-04-27
Note - short week for me as I'm taking an PTO day on Thursday and Family and Friends First day on Friday.
1. Y - [Cover for Thao who is on PTO](https://gitlab.com/gitlab-com/Product/-/issues/1048)
1. Y - Participate in the [Configure Design Sprint](https://gitlab.com/groups/gitlab-org/configure/-/epics/3)
1. Y - [Prep for new combined Ops Section GC](https://gitlab.com/gitlab-com/Product/-/issues/1108)
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. Y - Learning Goal - [Parent Child Pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)

## 2020-04-20
1. Y - [Cover for Thao who is on PTO](https://gitlab.com/gitlab-com/Product/-/issues/1048)
1. Y - Attend the Virtual CAB on 2020-04-21
1. Y - Attend Virtual Contribute on 2020-04-23
1. Y- [Communicate how our vision for a "Jenkins Importer" has undergone some iteration](https://gitlab.com/gitlab-com/Product/-/issues/1071) 
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. N - Learning Goal - [Parent Child Pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)

## 2020-04-13
1. Y - Prepare for [North Star Metric Review](https://gitlab.com/gitlab-com/Product/-/issues/880) - Ops on 2020-04-14, follow up from Review
1. Y - [Review all Group Planning issues](https://gitlab.com/gitlab-com/Product/-/issues/989) to continue getting up to speed
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. N - [CLeanup Ops Section Themes](https://gitlab.com/gitlab-com/Product/-/issues/994) as part of feedback from Direction review
1. Y - Learning Goal - [Multi-Project Pipelines ](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html)

## 2020-04-06
1. Y - Respond to request for [Ops Section Direction review from PM team](https://gitlab.com/gitlab-com/Product/-/issues/986)
1. N - [Review all Group Planning issues](https://gitlab.com/gitlab-com/Product/-/issues/989) to continue getting up to speed
1. N - [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951)
1. N - Prepare for [North Star Metric Review](https://gitlab.com/gitlab-com/Product/-/issues/880) - Ops on 2020-04-14

## 2020-03-30
1. Y - [Review to learn](https://gitlab.com/gitlab-com/Product/-/issues/947) - Review all Ops Section stage and category direction pages
1. Y - [Review all North Star Metric proposals for Product Groups](https://gitlab.com/gitlab-com/Product/-/issues/880)
1. Y - [Complete the combination of Ops and CI/CD section direction pages](https://gitlab.com/gitlab-com/Product/-/issues/912)
1. N - [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951)

## 2020-03-23
1. Y - Prepare and get up to speed for [Jason's parental leave](https://gitlab.com/gitlab-com/Product/-/issues/911)
1. Y - Make significant steps to [combining the Ops and CI/CD section direction pages](https://gitlab.com/gitlab-com/Product/-/issues/912)
1. Y - Checkin on team members as they find new normal
1. Y - Ensure we're advancing the [Adoption of North Star Metrics in Product Groups](https://gitlab.com/gitlab-com/Product/-/issues/880)
